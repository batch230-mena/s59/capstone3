/*
	Gitbash:
	npm init -y
	npm install express
	npm install mongoose
	npm install cors
    npm install jsonwebtoken
    npm install bcrypt
	touch .gitignore
*/

// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");

// Express server/application
const app = express();

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

// Connect to MongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch230.hqbff3v.mongodb.net/menaECommerce?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log("Now connected to Mena-Mongo DB Atlas"));

app.listen(process.env.PORT || 4000, () => { console.log(`Ecommerce API is now online on port ${process.env.PORT || 4000} `)
});
